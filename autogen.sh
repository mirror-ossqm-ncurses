#!/bin/bash

[ "$AUTOCONF" ]         || AUTOCONF=autoconf
[ "$AUTOHEADER" ]       || AUTOHEADER=autoheader
[ "$AUTOMAKE_VERSION" ] || AUTOMAKE_VERSION=1.10
[ "$AUTOMAKE_DATADIR" ] || AUTOMAKE_DATADIR=/usr/share/automake-$AUTOMAKE_VERSION
[ "$ACLOCAL" ]          || ACLOCAL=aclocal

rm -Rf autom4te.cache configure test/configure test/autom4te.cache aclocal.m4

## causes trouble w/ /usr/share/aclocal/libtool.m4
## $ACLOCAL     || exit 1

$AUTOHEADER  || exit 1
$AUTOCONF -i || exit 1
(cd test && $AUTOCONF) || exit 1

for f in install-sh config.sub config.guess mkinstalldirs ; do
    ln -sf $AUTOMAKE_DATADIR/$f
done
